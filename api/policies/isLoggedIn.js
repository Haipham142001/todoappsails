/**
 * isLoggedIn
 *
 * @module      :: Policy
 * @description :: TODO: You might write a short summary of how this policy works and what it represents here.
 * @help        :: http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  if (req.headers && req.headers.authorization) {
    const token = req.headers.authorization.slice(7);
    try {
      var checkAcToken = await AccessToken.findOne({ token: token });
      if (!checkAcToken) {
        return res.json({ message: 'Lỗi xác thực' });
      }
      var checkUser = await User.findOne({ id: checkAcToken.owner });
      if (!checkUser) {
        return res.json({ message: 'Từ chối truy cập' });
      }
      req.userID = checkUser.id;
      var checkRfToken = await RefreshToken.findOne({ owner: checkAcToken.owner });
      if (!checkRfToken) {
        return res.json({ message: 'Từ chối truy cập' });
      }
      const time = new Date();
      if (checkRfToken.timeCreate + checkRfToken.time < time.getTime()) {
        await AccessToken.destroy({ owner: checkAcToken.owner });
        await RefreshToken.destroy({ owner: checkAcToken.owner });
        return res.json({ message: 'Phiên đăng nhập hết hạn, đăng nhập lại' });
      }
      req.updateToken = false;
      if (checkAcToken.timeCreate + checkAcToken.time < time.getTime()) {
        var newAcToken = initToken.createToken(60);
        accessToken.update(checkAcToken.owner, newAcToken, time.getTime());
        req.newAcToken = newAcToken;
        req.updateToken = true;
      }
      return next();
    } catch (e) {
      return res.json({ message: 'Error' + e.error });
    }
  } else {
    return res.json({ message: 'Từ chối yêu cầu' });
  }
};
