/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var nodemailer = require('nodemailer');

module.exports = {
  register: async (req, res) => {
    const data = req.body;
    if (data.username !== '' && data.password !== '' && data.name !== '' && data.password.length >= 6 && data.email !== '') {
      try {
        var user = await User.findOne({ username: data.username });
        if (user) {
          return res.json({ message: 'Username đã được sử dụng' });
        }

        var email = await User.findOne({ email: data.email });
        if (email) {
          return res.json({ message: 'Email đã được sử dụng' });
        }

        await User.create({
          username: data.username,
          password: data.password,
          name: data.name,
          email: data.email
        });
        return res.json(200, { message: data.name + ' đã đăng ký thành công' });
      } catch (e) {
        return res.json(500, { message: e.erorr });
      }
    } else {
      var a = '';
      if (data.username === '') {
        a += 'tên đăng nhập, ';
      }
      if (data.password === '') {
        a += 'password, ';
      }
      if (data.password.length <= 6) {
        a += 'password phải nhiều hơn 6 ký tự, ';
      }
      if (data.name === '') {
        a += 'name, ';
      }
      if (data.email === '') {
        a += 'email.';
      }
      return res.json({ message: 'Thiếu ' + a });
    }
  },

  login: async (req, res) => {
    var d = new Date();
    var data = req.body;
    var user;
    var rfToken;

    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'haipham142001@gmail.com',
        pass: 'qkezilxnzcsuevwd'
      }
    });


    if (data.username !== '' && data.password !== '' && data.device !== '' && data.ip !== '') {
      try {
        user = await User.findOne({ username: data.username });

        // Check username
        if (!user) {
          return res.json({ message: data.username + ' không tồn tại' });
        }

        // Check password
        if (user.password !== data.password) {
          return res.json({ message: 'Sai mật khẩu' });
        }

        var { password, ...information } = user;

        // Check device
        var detectorDevice = await Device.findOne({ owner: user.id });
        if (detectorDevice) {

          var code = initToken.createVerifyCode(6);
          if (detectorDevice.publicIp !== data.ip || detectorDevice.userAgent !== data.device) {

            // Check verify code by user id
            var vCode = await VerifyCode.findOne({ owner: user.id });
            if (!vCode) {

              await VerifyCode.create({
                owner: user.id,
                code: code,
                timeCreate: d.getTime(),
                timeExpires: 3 * 60 * 1000 // 3 minutes
              });

              var mailOptions = {
                from: 'haipham142001@gmail.com',
                to: user.email,
                subject: 'Sending Email using Node.js',
                text: `Phát hiện tài khoản đang được đăng nhập tại một nơi khác. Mã xác thực: ${code}. Nếu không phải là bạn, hãy bỏ qua tin nhắn này.`
              };

              transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });

              // sails.hooks.email.send(
              //   'testEmail',
              //   {
              //     recipientName: 'qq',
              //     senderName: 'aa'
              //   },
              //   {
              //     to: user.email,
              //     subject: 'Hi there'
              //   },
              //   (err) => {
              //     console.log(err || 'It worked!');
              //   }
              // );

              return res.json({ message: `Phát hiện đăng nhập trên thiết bị khác. Chúng tôi đã gửi một mã xác nhận tới ${user.email}`, type: user.email });
            } else {
              if (data.verifyCode) {
                if (data.verifyCode !== vCode.code) {
                  return res.json({ message: 'Sai xác thực' });
                }
                if (vCode.timeCreate + vCode.timeExpires < d.getTime()) {
                  await VerifyCode.destroy({ owner: user.id });
                  return res.json({ message: 'Hết thời gian hiệu lực' });
                }
              } else {
                if (vCode.timeCreate + vCode.timeExpires < d.getTime()) {
                  await VerifyCode.destroy({ owner: user.id });
                  return res.json({ message: 'Hết thời gian hiệu lực' });
                }
                return res.json({ message: 'Chưa xác thực' });
              }
            }
          }
        } else {
          await Device.create({
            publicIp: data.ip,
            userAgent: data.device,
            owner: user.id
          });
        }

        var actoken = initToken.createToken(60);
        var rftoken = initToken.createToken(60);
        var secret = initToken.createToken(10);

        // Check refresh token
        rfToken = await RefreshToken.findOne({ owner: user.id });
        if (!rfToken) {
          var acTokenNew = initToken.createToken(60);
          refreshToken.create(user.id, rftoken, 60 * 60 * 1000, secret);
          accessToken.create(user.id, acTokenNew, 12 * 60 * 1000, secret, d.getTime());
          return res.json(200, { token: acTokenNew, user: information });
        }

        // Check time of refresh token
        // refesh token is exists
        if (rfToken.timeCreate + rfToken.time > d.getTime()) {

          // return refresh token
          var checkAcToken = await AccessToken.findOne({ owner: user.id });
          if (!checkAcToken) {
            accessToken.create(user.id, actoken, 12 * 60 * 1000, secret);
          }

          if (checkAcToken.createTime + checkAcToken.time < d.getTime()) {
            accessToken.update(user.id, actoken);
          }
          return res.json(200, { token: checkAcToken.token, user: information });
        }

        // Expiry time refresh token
        // Delete login session and request cilent re-login
        refreshToken.delete(user.id);
        accessToken.delete(user.id);

        return res.json(200, { message: 'Phiên đăng nhập hết hạn. Đăng nhập lại' });
      } catch (e) {
        return res.json({ message: 'Lỗi: ' + e.error });
      }
    }
  },

  logout: async (req, res) => {
    var data = req.headers;
    try {
      if (data.token) {
        var acToken = await AccessToken.findOne({ token: data.token });
        if (acToken) {
          await AccessToken.destroy({ owner: acToken.owner });
          var rfToken = await RefreshToken.findOne({ owner: acToken.owner });
          if (rfToken) {
            await RefreshToken.destroy({ owner: acToken.owner });
          }
          var vCode = await VerifyCode.findOne({ owner: acToken.owner });
          if (vCode) {
            await VerifyCode.destroy({ owner: acToken.owner });
          }
          return res.json({ message: 'Đăng xuất thành công' });
        } else {
          return res.json({ message: 'Từ chối yêu cầu đăng xuất' });
        }
      }
    } catch (err) {
      return res.json(400, { message: err.error });
    }
  }

};
