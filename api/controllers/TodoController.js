/**
 * TodoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    var data = req.body;
    try {
      let justCreated = await Todo.create({
        name: data.name,
        dateStart: data.dateStart,
        deadline: data.deadline,
        status: false,
        userID: req.userID
      }).fetch();
      if (req.updateToken === true) {
        return res.json(200, { message: 'Tạo mới hoàn tất', todo: justCreated, token: req.newAcToken });
      } else {
        return res.json(200, { message: 'Tạo mới hoàn tất', todo: justCreated });
      }
    } catch (e) {
      return res.json(500, { message: e.error });
    }
  },

  list: async (req, res) => {
    try {
      var listTodo = await Todo.find({ userID: req.userID });
      if (listTodo.length <= 0) {
        return res.json(200, { message: 'no task', token: req.newAcToken });
      }
      if (req.updateToken === true) {
        return res.json(200, { todos: listTodo, token: req.newAcToken });
      } else {
        return res.json(200, { todos: listTodo, });
      }
    } catch (e) {
      return res.json(500, { message: e.error });
    }
  },

  update: async (req, res) => {
    var data = req.body;
    try {
      const updateTodod = await Todo.update({ id: data.id })
        .set({
          name: data.name,
          dateStart: data.dateStart,
          deadline: data.deadline,
          status: data.status,
        }).fetch();
      if (req.updateToken === true) {
        return res.json(200, { message: 'Cập nhật hoàn tất', todo: updateTodod, token: req.newAcToken });
      } else {
        return res.json(200, { message: 'Cập nhật hoàn tất', todo: updateTodod });
      }
    } catch (e) {
      return res.json(500, { message: e.error });
    }
  },

  delete: async (req, res) => {
    var data = req.body;
    try {
      await Todo.destroy({ id: data.id });
      if (req.updateToken === true) {
        return res.json(200, { message: 'Xóa hoàn tất', token: req.newAcToken });
      } else {
        return res.json(200, { message: 'Xóa hoàn tất' });
      }
    } catch (e) {
      return res.json(500, { message: e.error });
    }
  }
};

